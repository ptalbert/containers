#define _USE_YUM_PKG_MANAGER 1

/* DO NOT use Dockerfile/Containerfile commands before this line */
#include "setup-from-rhel"

#include "builder-all"

#if (_RHEL_MINOR_VERSION < 7)
/* Install compatible Python 2 + 3 versions missing/conflicting at RHEL 7.Z where Z < 7 */
RUN yum --enablerepo=rhel7.7-missing --enablerepo=rhel7.7-missing-optional -y install \
      python \
      python-devel \
      python3 \
      python3-devel \
      python3-pip
#endif

/* Install the required packages for CI work. */
RUN yum -y install \
      htop \
      python \
      python-docutils \
      yum-utils

RUN yum -y install \
      binutils-aarch64-linux-gnu gcc-aarch64-linux-gnu \
      binutils-powerpc64-linux-gnu gcc-powerpc64-linux-gnu \
      binutils-powerpc64le-linux-gnu \
      binutils-s390x-linux-gnu gcc-s390x-linux-gnu

/* There's only one cross compiler binary for both ppc64 and ppc64le. Kernel
 * can handle this special case but we still want correct compiler data to submit.
 * We can't symlink the two because gcc is special and overrides the target name
 * with the symlink name, likely due to how the cross compilers are actually
 * implemented as part of gcc. This is the case for all gcc packages and is not
 * RHEL7 specific:
 *
 * $ which gcc
 * /usr/bin/gcc
 * $ ln -s /usr/bin/gcc mytestfile
 * $ ./mytestfile --version
 * mytestfile (GCC) 10.2.1 20201016 (Red Hat 10.2.1-6)          <----- mytestfile
 * Copyright (C) 2020 Free Software Foundation, Inc.
 * This is free software; see the source for copying conditions.  There is NO
 * warranty; not even for MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.
 *
 * Because of this peculiarity, we create a shell script that invokes the correct
 * compiler instead.
 */
RUN echo -e '#!/bin/bash\nexec /usr/bin/powerpc64-linux-gnu-gcc "$@"' > /usr/bin/powerpc64le-linux-gnu-gcc \
      chmod +x /usr/bin/powerpc64le-linux-gnu-gcc

RUN yum-builddep -y kernel

/* Download and install backports */
#include "snippet-backported-bash-rhel"
#include "snippet-backported-python-rhel"

/* Install AWS cli via pip to talk to S3. */
RUN python3 -m pip install --ignore-installed awscli

#include "cleanup"
